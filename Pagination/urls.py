from django.contrib import admin
from django.urls import path
from paginapi import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('studentapi/', views.StudentList.as_view())
]