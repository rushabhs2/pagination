from django.shortcuts import render

from paginapi.mypagination import MyPageNumberPagination
from .serializers import StudentSerializer
from .models import Students
from rest_framework.generics import ListAPIView
from paginapi.mypagination import MyPageNumberPagination


# Create your views here.

class StudentList(ListAPIView):
    queryset = Students.objects.all()
    serializer_class = StudentSerializer
    pagination_class = MyPageNumberPagination
