from django.apps import AppConfig


class PaginapiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'paginapi'
